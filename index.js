const { JSDOM } = require('jsdom')
const jquery = require('jquery')
const fs = require('fs')
const iconv = require('iconv-lite')

const data_path = './site_data/excerpts.htm'

let rs = fs.createReadStream(data_path)
let decoder = iconv.decodeStream('win1252')

rs.pipe(decoder)

decoder.collect((err,data) => {
  let DOM = new JSDOM(data)
	$ = jquery(DOM.window)

  $('meta').remove()
	$('style').remove()
	$('[class]').removeAttr('class')
	$('[style]').removeAttr('style')

  $('body').html(
    $('div:first').html().replace(/&nbsp;/gi, ' ')
  )

  //remove top level whitespace text nodes
	$('head, body')
		.contents()
		.filter(function() {
			return this.nodeType === 3 && /\s+/.test(this.nodeValue); //Node.TEXT_NODE
		}).remove()

	$('span').replaceWith(function() { return this.innerHTML })

	$('p:contains(Session)')
		.addClass('session-id')

	$('p:contains(Q:)')
		.addClass('question')

	$('.session-id')
		.each( function (index, element) {
			const group = $(element).nextUntil('.session-id').addBack()
			const wrapper = $('<div />', {class: 'excerpt'})
			group.wrapAll(wrapper)
		})

  // fix for bad character interpretation.
  $('head').append('<meta charset="UTF-8" />')

  // improve source readability
  $('.excerpt, .excerpt p').before('\n\n')

	fs.writeFileSync('./output.htm'
      , "<!DOCTYPE html>\n" + DOM.window.document.documentElement.outerHTML
      , {
        encoding:'utf8'
      }
    )

})
